const { callbackify } = require('util');

const app = require('express')();

const server = require('http').createServer(app);

const  { addUser, removeUser, getUser, getUsersInRoom } = require('./users')

// for cors error handling
const io = require('socket.io')(server, {
    cors: {
        origin: '*'
    }
})

const PORT = process.env.PORT || 5000;


io.on('connection', (socket) => {
	socket.on('join', ({name, room}, callback) => {
		const {error, user} = addUser({ id: socket.id, name, room});

		if(error) return callback(error);
		socket.emit('message', {user: 'admin', text: `${name}, welcome to the ${room}`});
		socket.broadcast.to(user.room).emit('message', {user: 'admin', text: `${name} has joined.`}); 

		socket.join(user.room);
		
		io.to(user.room).emit('roomData', {room: user.room, user: getUsersInRoom(user.room)})
		callback();
	})

	socket.on('sendMessage', (message, callback)=> {
		const user = getUser(socket.id);

		io.to(user.room).emit('message', { user: user.name, text: message});
		io.to(user.room).emit('roomData', {room: user.room, user: getUsersInRoom(user.room)});


		callback();
	})
	socket.on('disconnect', () => {
		const user = removeUser(socket.id);

		if(user){
			io.to(user.room).emit('message', {user: 'admin', text: `${user.name} has left the room.`})
		}
	})
})
server.listen(PORT, () => console.log(`Server is listening at port ${PORT}`))


